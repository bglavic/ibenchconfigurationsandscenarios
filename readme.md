# Introduction

This is a public project used as a central repository for configuration files for the iBench metadata generator and integration scenarios in the format supported by iBench. This repository is meant to ease sharing of metadata used for evaluating data integration systems.

# Instructions

If you upload your own iBench configuration file or scenario XML document, we kindly ask you to add a section to the bottom of this document describing the purpose and characteristics of the file and potentially its purpose, i.e., what evaluation you were using the generated metadata for.

# Available Configurations



# Available XML Integration Scenarios
